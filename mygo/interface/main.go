package main

type bot interface{
	greet()string
}

type englishBot struct{}
type dzongkhaBot struct{}

func main() {
	
}

func (englishBot) greet() string {
	return "hello"// custom logic
}

//you can ignore reciever variable if it is not used in the function
func (dzongkhaBot) greet() string {
	return "kuzu la" //custom logic
}