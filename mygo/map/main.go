package main
// import "fmt"
func main() {
	//declare and assign values to map
	colors := map[string]string{
		"red": "danger",
		"green": "nature",
		"blue": "sky",
	}
	printMap(colors)
}

func printMap(c map[string]string){
	for color, def := range c {
		println("defination of "+ color + " is " + def)
	}
}
