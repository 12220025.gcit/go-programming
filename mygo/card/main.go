package main

import "fmt"

func main() {
	// card := deck{"K","Q","J", }
	// card = append(card, "six of spade")
	// card.print()
	// new := newDeck()
	// new.print()

	//returing deal
	// card :=newDeck()
	//  hand, remainingCards :=  deal(card, 5)
	//  hand.print()
	//  remainingCards.print()

	//type conversion
	card := newDeck()
	fmt.Println(card.toString())
	card.saveToFile("deck1")

	//reading the file from local using newDeckfromfile function
	//also to convert the byteslice back to deck type
	// myCard := newDeck()
	// myCard = newDeckfromFile("deck1")
	// myCard.print()

	//Shuffling the cards
	//Creating a new deck of cards to shuffle
	shuffledCard := newDeck()
	shuffledCard.print()

	//shuffling the new created deck of cards
	shuffledCard.shuffle()
	shuffledCard.print()

	//     fmt.Println(len(new))
	// 	// fmt.Println(card)
	//    for i, card :=range card{
	// 	fmt.Println(i,card)
}

//    var s []string
//     s = make([]string, 4, 7)
// 	append(card,)
// 	fmt.Println(s)

// }

// func newCard() string{
// 	return "ace of diamond"
// }
