package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
)

type deck []string

func (d deck) print() {
  for i, card :=range d{
	fmt.Println(i, card)
  }
}

func newDeck() deck {
	card := deck{}
	cardSuits := []string{"spade", "heart", "diamond", "clubs"}
	cardValues := []string{"Ace", "king", "Queen",
		"Joker"}

	for _, suit := range cardSuits {
		for _, value := range cardValues {
			card = append(card, value+" of "+suit)
		}
	}
	return card

}

// func deal(d deck, handsize int)(deck, deck) {
// 	return d[:handsize], d[handsize:]
// }

//helper func

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}
func (d deck) saveToFile(fileName string) error {
	return os.WriteFile(fileName, []byte(d.toString()), 0666)
}

//we are changing the byte slice into deck  type

// reading func
func newDeckfromFile(filename string) deck {
	byteslice, err := os.ReadFile(filename)

	if err != nil {
		fmt.Println("Error", err)
		os.Exit(1)
	}
	// convert byteslice to deck
	str := strings.Split(string(byteslice), ",")
	return deck(str)
}

func (d deck) shuffle() {
	for i := range d {
		//generates new random index
		newIndex := rand.Intn(len(d) - 1)

		//swap the position of the indexes.
		d[i], d[newIndex] = d[newIndex], d[i]
	}
}
